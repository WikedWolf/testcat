from django.contrib.auth.models import User
from django.contrib.auth import get_user_model

from rest_framework import serializers

from api.models import Cat, Breed


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)

    def create(self, validated_data):
        user = get_user_model().objects.create_user(**validated_data)
        return user

    def update(self, instance, validated_data):
        if 'password' in validated_data:
            password = validated_data.pop('password')
            instance.set_password(password)
        return super(UserSerializer, self).update(instance, validated_data)

    class Meta:
        model = get_user_model()
        fields = ('id', 'username', 'first_name', 'last_name', 'password')
        extra_kwargs = {
            'password': {'write_only': True},
        }


class BreedSerializer(serializers.ModelSerializer):
    class Meta:
        model = Breed
        fields = ('id', 'name', 'description', 'photo')	
        extra_kwargs = {
            'photo': {'read_only': True},
        }


class CatSerializer(serializers.ModelSerializer):
    # owner = UserSerializer(many=False)
    breed = BreedSerializer(read_only=True)
    breed_id = serializers.IntegerField()
        

    def create(self, validated_data):
        validated_data['breed'] = validated_data['breed']['id']
        cat = Cat.objects.create(**validated_data)
        return cat

    def update(self, instance, validated_data):
        print(validated_data)
        instance.breed_id = validated_data.get('breed_id', instance.breed_id)
    
        return super(CatSerializer, self).update(instance, validated_data)

    class Meta:
        model = Cat
        fields = ('id', 'name', 'biography', 'birthday', 'breed', 'breed_id')
       
