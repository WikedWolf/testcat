from rest_framework import routers
from django.urls import path, include
from . import views
 
router = routers.DefaultRouter(trailing_slash=False)
router.register(r'cats/', views.CatViewSet)
router.register(r'breeds/', views.BreedViewSet)
router.register(r'users/', views.UserViewSet)
 
urlpatterns = [
    path(r'', include(router.urls)),
]
