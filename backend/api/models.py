from django.db import models
from django.contrib.auth.models import User


class Breed(models.Model):
	name = models.CharField(verbose_name='Breed name', max_length=20)
	description = models.TextField(verbose_name='Description of breed')
	photo = models.ImageField(verbose_name='Photo', upload_to='breeds/')

	def __str__(self):
		return self.name

class Cat(models.Model):
	name = models.CharField(verbose_name='Name', max_length=20)
	biography = models.TextField(verbose_name='Biography', null=True, blank=True)
	birthday = models.DateField(verbose_name='Age')
	breed = models.ForeignKey(Breed, verbose_name='Cat breed', on_delete=models.CASCADE)
	owner = models.ForeignKey(User, verbose_name='Cat owner', null=True, blank=True, on_delete=models.CASCADE)
	def pre_save(self, request):
		self.owner = request.user
	def __str__(self):
		return self.name

