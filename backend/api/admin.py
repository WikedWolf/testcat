from django.contrib import admin
from api.models import Cat, Breed

class CatAdmin(admin.ModelAdmin):
    pass

class BreedAdmin(admin.ModelAdmin):
    pass
    

admin.site.register(Cat, CatAdmin)
admin.site.register(Breed, BreedAdmin)
