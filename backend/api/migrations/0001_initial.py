# Generated by Django 3.1.7 on 2021-03-31 14:55

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Breed',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=20, verbose_name='Breed name')),
                ('description', models.TextField(verbose_name='Description of breed')),
                ('photo', models.ImageField(upload_to='breeds/', verbose_name='Photo')),
            ],
        ),
        migrations.CreateModel(
            name='Cat',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=20, verbose_name='Name')),
                ('birthday', models.DateField(verbose_name='Age')),
                ('breed', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.breed', verbose_name='Cat breed')),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Cat owner')),
            ],
        ),
    ]
