import {Component} from '@angular/core';
import {ApiService} from '../../api/services/api.service';
import {formatDate} from '@angular/common';


@Component({
  selector: 'cat',
  providers: [ApiService],
  templateUrl: '../templates/cat.component.html',
  styleUrls: ['../styles/cat.component.css']
})
 
export class CatComponent {
  public isLoggedIn:any = false;
  public regForm:boolean = false;

  public creeds:any = {};
  public reguser:any = {};
  public newcat:any = {};
  public hide:boolean = true;
  public cats:any = []
  public showForm:boolean = false;
  public breeds:any = [];

  constructor(private _service: ApiService) { }
 
  ngOnInit() {
    console.log('Init component')
    this.isLoggedIn = this._service.checkCredentials(); 
    if (this.isLoggedIn){
    	this._service.getResource('/breeds/').subscribe(
          data => {this.breeds = data},
          err => alert('Unhandled error: '+err.detail	)
    	);
    }   
  }

  login() {
  	console.log(this.creeds)
  	
    if(!this.isLoggedIn) {
      this._service.retrieveToken(this.creeds);
    }
 }
  logout() {
    this._service.logout();
  }

  registration() {
  	this._service.postResource('/users/', this.reguser).subscribe(
          data => {this.regForm = false;},
          err => console.log('Unhandled error: '+err.detail)
    	);

  }


  getCats() {
  	this.showForm = false;
  	this._service.getResource('/cats/').subscribe(
          data => {this.cats = data},
          err => alert('Unhandled error')
    ); 
  };
  newCat() {
  	this.cats = []
  	this.showForm = true;
  	let breeds = [];
  }
  editCat(item:any) {
  	this.newcat = item
  	this.cats = []
  	this.showForm = true
  }
  saveCat() {
  	this.newcat.birthday = formatDate(this.newcat.birthday, 'YYYY-MM-dd', 'ru-RU')
  	this.newcat.breed_id = this.newcat.breed.id
  	if (this.newcat.id){
  		this._service.updateResource('/cats/', this.newcat, true).subscribe(
          data => {this.newcat = {}; this.getCats();},
          err => console.log('Unhandled error: '+err.detail)
    	);	
  	}else{
  		this._service.postResource('/cats/', this.newcat, true).subscribe(
          data => {this.newcat = {}; this.getCats();},
          err => console.log('Unhandled error: '+err.detail)
    	);	
  	}
  	
  }
  
  	
  

}