import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { Observable } from  'rxjs';

@Injectable()
export class ApiService {
  public clientId = 'bzlhHzPXx3r9KbaiuM6bOUN58YKG15y2g5qyxwPG';
  private clientSecret = 'oueLoJij0XcvJFg3qxkaXECQ2usknQxYC8hb3IuYnkcck588CMDyR6rkkVkREvWXrILFYa0Q8pnQcc9y79H5xC2Li0PpEG8kpfRwDojCcirT3IBrd3375xVFGRzscyEx';
  public redirectUri = 'http://localhost:4200/';
  public apiDomain = 'http://34.82.166.48/backend';
  constructor(private _http: HttpClient) { }

  retrieveToken(creeds:any) {
    let params = new URLSearchParams();   
    params.append('grant_type','password');
    params.append('client_id', this.clientId);
    params.append('client_secret', this.clientSecret);
    params.append('redirect_uri', this.redirectUri);
    params.append('username', creeds.username);
    params.append('password', creeds.password);

    let headers = 
      new HttpHeaders({'Content-type': 'application/x-www-form-urlencoded; charset=utf-8'});
       
      this._http.post(this.apiDomain + '/o/token/', 
        params.toString(), { headers: headers })
        .subscribe(
          data => this.saveToken(data),
          err => alert('Invalid Credentials')); 
  }

  saveToken(token:any) {
    let expireDate = new Date().getTime() + (1000 * token.expires_in);
    Cookie.set("access_token", token.access_token, expireDate);
    console.log('Obtained Access token');
    window.location.href = 'http://localhost:4200';
  }

  getResource(resourceUrl:string) : Observable<any> {
    let headers = new HttpHeaders({
      'Content-type': 'application/json; charset=utf-8', 
      'Authorization': 'Bearer '+this.getToken()});
    return this._http.get(this.apiDomain+'/api'+resourceUrl, { headers: headers });
  }

  postResource(resourceUrl:string, obj:any = {}, with_creeds:boolean = false) : Observable<any> {
    let headerDict:any = {
      'Content-type': 'application/json; charset=utf-8'
    }
    if (with_creeds){
      headerDict['Authorization'] = 'Bearer '+this.getToken()
    }
    let headers = new HttpHeaders(headerDict);
    return this._http.post(this.apiDomain + '/api'+ resourceUrl, 
        obj, { headers: headers })
  }
  updateResource(resourceUrl:string, obj:any = {}, with_creeds:boolean = false) : Observable<any> {
    let headerDict:any = {
      'Content-type': 'application/json; charset=utf-8'
    }
    if (with_creeds){
      headerDict['Authorization'] = 'Bearer '+this.getToken()
    }
    let headers = new HttpHeaders(headerDict);
    return this._http.put(this.apiDomain + '/api'+ resourceUrl, 
        obj, { headers: headers })
  }
  
  getToken() {
  	return Cookie.get('access_token');
  }

  checkCredentials() {

  	let tkn = this.getToken();
  	console.log(tkn)
  	return tkn?true:false;
  } 

  logout() {
    Cookie.delete('access_token');
    window.location.reload();
  }
}